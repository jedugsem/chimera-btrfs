#!/bin/sh
. ./vars
. ./utils.sh
ROOT="/mnt"
CRYPT_DEVICE="/dev/mapper/$CRYPT_NAME"

create_part

mount_part

apk --root $ROOT --keys-dir /etc/apk/keys --repositories-file /etc/apk/repositories.d/00-repo-main.list --initdb add chimerautils

mount -t proc none $ROOT/proc
mount -t sysfs none $ROOT/sys
mount -t devtmpfs none $ROOT/dev
mount --bind /tmp $ROOT/tmp

mkdir -p $ROOT/etc/apk/keys
cp /etc/apk/keys/*.pub $ROOT/etc/apk/keys

apk --root $ROOT --repositories-file /etc/apk/repositories.d/00-repo-main.list add base-full

umount $ROOT/tmp
umount $ROOT/dev
umount $ROOT/sys
umount $ROOT/proc
rm -rf $ROOT/run/ $ROOT/var/tmp/* $ROOT/var/cache/*
mkdir -p $ROOT/run
chmod 777 $ROOT/var/tmp

#$BASE

mkdir /mnt/home/installer
cp -r ./* /mnt/home/installer/
#chimera-live-chroot /mnt /bin/sh /home/installer/chroot-script.sh

#umount -R /mnt
#umount /mnt
