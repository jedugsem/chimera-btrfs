#!/bin/sh
. /home/installer/vars

# FSTAB
LUKS_UUID=$(blkid -s UUID -o value /dev/mapper/$CRYPT_NAME)
UUID=$(blkid -s UUID -o value "$DRIVE""$LUKS_DISK")
EFI_UUID=$(blkid -s UUID -o value "$DRIVE""$EFI_DISK")
echo "# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
UUID=${LUKS_UUID} / btrfs rw,noatime,$MOUNT_OPTS 0 0
UUID=${LUKS_UUID} /home btrfs rw,noatime,$MOUNT_OPTS,subvol=/@/home 0 0
UUID=${LUKS_UUID} /opt btrfs rw,noatime,$MOUNT_OPTS,subvol=/@/opt 0 0
UUID=${LUKS_UUID} /root btrfs rw,noatime,$MOUNT_OPTS,subvol=/@/root 0 0
UUID=${LUKS_UUID} /srv btrfs rw,noatime,$MOUNT_OPTS,subvol=/@/srv 0 0
UUID=${LUKS_UUID} /tmp btrfs rw,noatime,$MOUNT_OPTS,subvol=/@/tmp 0 0
UUID=${LUKS_UUID} /usr/local btrfs rw,noatime,$MOUNT_OPTS,subvol=@/usr/local 0 0
UUID=${LUKS_UUID} /var/cache btrfs rw,noatime,$MOUNT_OPTS,subvol=@/var/cache 0 0
UUID=${LUKS_UUID} /var/log btrfs rw,noatime,$MOUNT_OPTS,subvol=@/var/log 0 0
UUID=${LUKS_UUID} /var/spool btrfs rw,noatime,$MOUNT_OPTS,subvol=@/var/spool 0 0
UUID=${LUKS_UUID} /var/lib btrfs rw,noatime,$MOUNT_OPTS,subvol=@/var/lib 0 0
UUID=${LUKS_UUID} /var/tmp btrfs rw,noatime,$MOUNT_OPTS,subvol=@/var/tmp 0 0
UUID=${LUKS_UUID} /boot/grub btrfs rw,noatime,$MOUNT_OPTS,subvol=@/boot/grub 0 0
UUID=${LUKS_UUID} /.snapshots btrfs rw,noatime,$MOUNT_OPTS,subvol=@/.snapshots 0 0
UUID=${EFI_UUID} /efi vfat defaults 0 2
" > /etc/fstab
#UUID=${LUKS_UUID} /var/swap btrfs rw,noatime,subvol=@/var/swap 0 0

apk add grub grub-x86_64-efi os-prober efibootmgr linux-lts cryptsetup-scripts
#snapper
mkdir -m 0700 /etc/keys
dd bs=512 count=4 if=/dev/random of=/etc/keys/root.key iflag=fullblock
cryptsetup luksAddKey "$DRIVE""$LUKS_DISK" /etc/keys/root.key
echo "$CRYPT_NAME UUID=$UUID /etc/keys/root.key luks,discard,key-slot=0" > /etc/crypttab
echo UMASK=0077 >> /etc/initramfs-tools/initramfs.conf
echo "KEYFILE_PATTERN=\"/etc/keys/*.key\"" >> /etc/cryptsetup-initramfs/conf-hook
update-initramfs -c -k all


# Lokalisierung
# ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
# sed -i 's/#de_DE/de_DE/' /etc/locale.gen
# locale-gen
# echo "export LANG=\"de_DE.UTF-8\"" >> /etc/locale.conf

# Snapper initialisierung
# umount /.snapshots
# rm -r /.snapshots
# snapper --no-dbus -c root create-config /
# btrfs subvolume delete /.snapshots
# mkdir /.snapshots
# mount -o compress=zstd,subvol=@/.snapshots /dev/mapper/cryptroot /.snapshots
# chmod 750 /.snapshots

# Grub einstellungen und Installation
echo "Grub Initialisierung"
# Modifiziere Grub Routine, damit Grub das standart Subvolume startet
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/20_linux_xen
sed -i "s/rootflags=subvol=${rootsubvol}/    /g" /etc/grub.d/10_linux
# Modifiziere Grub Configurationsdateien
echo "GRUB_ENABLE_CRYPTODISK=y" >> /etc/default/grub
echo "GRUB_CMDLINE_LINUX_DEFAULT=\"cryptdevice=UUID=${UUID}:cryptroot:allow-discards\"" >> /etc/default/grub
# Verändere die Initramfs Einstellungen
#sed -i 's/HOOKS=(.*)/HOOKS=(base udev autodetect modconf block encrypt keyboard keymap consolefont filesystems)/g' /etc/mkinitcpio.conf
#sed -i 's/FILES=()/FILES=(\/crypto_keyfile.bin)/g' /etc/mkinitcpio.conf


echo "doas setup missing the sudo removal"
echo "permit persist :wheel as root
  permit nopass root as $USERNAME" >> /etc/doas.conf

#mkinitramfs

grub-install --target=x86_64-efi --efi-directory=/efi
grub-mkconfig -o /boot/grub/grub.cfg

# Hostnames
echo $HOSTNAME > /etc/hostname
echo "127.0.0.1        localhost
 ::1              localhost
 127.0.1.1        ${HOSTNAME}.local  ${HOSTNAME}" > /etc/hosts

# Nutzersetup
echo "root password"
passwd
useradd -m $USERNAME
usermod -aG audio,input,video,network,wheel $USERNAME
echo "passwd $USERNAME"
passwd $USERNAME
