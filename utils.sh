#!/bin/sh
create_part() {
    fdisk $DRIVE
    cryptsetup luksFormat --type luks1 --iter-time $CRYPT_ITER "$DRIVE""$LUKS_DISK"
    cryptsetup open "$DRIVE""$LUKS_DISK" $CRYPT_NAME
    umount -Rf /mnt
    rm -rf /mnt/*
    fdisk -l $DRIVE
    mkfs.vfat -F32 "$DRIVE""$EFI_DISK"
    mkfs.btrfs -f -L VERA -n 32k $CRYPT_DEVICE
    #mount -o "$MOUNT_OPTS" $CRYPT_DEVICE /mnt/
    mount $CRYPT_DEVICE /mnt/

    btrfs subvolume create /mnt/@
    
    mkdir /mnt/@/boot
    btrfs subvolume create /mnt/@/boot/grub
    btrfs subvolume create /mnt/@/.snapshots
    
    mkdir /mnt/@/.snapshots/1
    btrfs subvolume create /mnt/@/.snapshots/1/snapshot
    btrfs subvolume create /mnt/@/home
    btrfs subvolume create /mnt/@/opt
    btrfs subvolume create /mnt/@/root
    btrfs subvolume create /mnt/@/srv
    btrfs subvolume create /mnt/@/tmp
    
    mkdir /mnt/@/usr/
    btrfs subvolume create /mnt/@/usr/local
    
    mkdir /mnt/@/var/
    btrfs subvolume create /mnt/@/var/cache
    btrfs subvolume create /mnt/@/var/log
    btrfs subvolume create /mnt/@/var/spool
    btrfs subvolume create /mnt/@/var/tmp
    btrfs subvolume create /mnt/@/var/lib
    #btrfs subvolume create /mnt/@/var/swap

    chattr +C /mnt/@/var/lib
    chattr +C /mnt/@/var/spool
    chattr +C /mnt/@/var/cache
    chattr +C /mnt/@/var/tmp
    chattr +C /mnt/@/var/log
    #chattr +C /mnt/@/var/swap

    echo "<?xml version=\"1.0\"?>
    <snapshot>
        <type>single</type>
        <num>1</num>
        <date>$(date +"%Y-%m-%d %H:%M:%S")</date>
        <description>First Snapshot</description>
    </snapshot>" > /mnt/@/.snapshots/1/info.xml

    btrfs subvolume set-default $(btrfs subvolume list /mnt | grep "@/.snapshots/1/snapshot" | sed 's/ID //' | sed 's/ gen.*//') /mnt
    umount /mnt
}

mount_part() {
    mount -o $MOUNT_OPTS $CRYPT_DEVICE /mnt
    mkdir -p /mnt/boot/grub
    mkdir /mnt/.snapshots
    mkdir /mnt/home
    mkdir /mnt/opt
    mkdir /mnt/root
    mkdir /mnt/srv
    mkdir /mnt/tmp
    mkdir -p /mnt/usr/local
    mkdir /mnt/var
    mkdir /mnt/var/spool
    mkdir /mnt/var/lib
    mkdir /mnt/var/log
    mkdir /mnt/var/cache
    mkdir /mnt/var/tmp
    #mkdir /mnt/var/swap

    mount -o "$MOUNT_OPTS",subvol=@/.snapshots $CRYPT_DEVICE /mnt/.snapshots 
    mount -o "$MOUNT_OPTS",subvol=@/home $CRYPT_DEVICE /mnt/home 
    mount -o "$MOUNT_OPTS",subvol=@/opt $CRYPT_DEVICE /mnt/opt
    mount -o "$MOUNT_OPTS",subvol=@/root $CRYPT_DEVICE /mnt/root 
    mount -o "$MOUNT_OPTS",subvol=@/srv $CRYPT_DEVICE /mnt/srv
    mount -o "$MOUNT_OPTS",subvol=@/tmp $CRYPT_DEVICE /mnt/tmp 
    mount -o "$MOUNT_OPTS",subvol=@/usr/local $CRYPT_DEVICE /mnt/usr/local
    mount -o "$MOUNT_OPTS",subvol=@/var/cache $CRYPT_DEVICE /mnt/var/cache
    mount -o "$MOUNT_OPTS",subvol=@/var/log $CRYPT_DEVICE /mnt/var/log
    mount -o "$MOUNT_OPTS",subvol=@/var/spool $CRYPT_DEVICE /mnt/var/spool
    mount -o "$MOUNT_OPTS",subvol=@/var/lib $CRYPT_DEVICE /mnt/var/lib
    mount -o "$MOUNT_OPTS",subvol=@/var/tmp $CRYPT_DEVICE /mnt/var/tmp
    #mount -o subvol=@/var/swap $CRYPT_DEVICE /mnt/var/swap
    mount -o "$MOUNT_OPTS",subvol=@/boot/grub $CRYPT_DEVICE /mnt/boot/grub

    mkdir /mnt/efi/
    mount "$DRIVE""$EFI_DISK" /mnt/efi

    dd bs=512 count=4 if=/dev/random of=/mnt/crypto_keyfile.bin iflag=fullblock
    chmod 000 /mnt/crypto_keyfile.bin
    cryptsetup -v luksAddKey "$DRIVE""$LUKS_DISK" /mnt/crypto_keyfile.bin
}
